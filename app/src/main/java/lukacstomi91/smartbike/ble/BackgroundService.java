package lukacstomi91.smartbike.ble;

/**
 * Created by Tom on 2016. 05. 07..
 */

        import android.app.Service;
        import android.bluetooth.BluetoothAdapter;
        import android.bluetooth.BluetoothDevice;
        import android.bluetooth.BluetoothGatt;
        import android.bluetooth.BluetoothGattCallback;
        import android.bluetooth.BluetoothGattCharacteristic;
        import android.bluetooth.BluetoothGattDescriptor;
        import android.bluetooth.BluetoothGattService;
        import android.bluetooth.BluetoothManager;
        import android.bluetooth.BluetoothProfile;
        import android.content.BroadcastReceiver;
        import android.content.Context;
        import android.content.Intent;
        import android.content.IntentFilter;
        import android.os.Handler;
        import android.os.IBinder;
        import android.text.format.DateUtils;
        import android.util.Log;
        import android.widget.Toast;

        import com.movisens.smartgattlib.Characteristic;
        import com.movisens.smartgattlib.Descriptor;
        import com.opencsv.CSVWriter;

        import java.io.IOException;
        import java.math.BigInteger;
        import java.util.ArrayList;
        import java.util.Arrays;
        import java.util.HashMap;
        import java.util.List;
        import java.util.UUID;


        import java.io.FileWriter;

        import lukacstomi91.smartbike.R;
        import lukacstomi91.smartbike.activity.SetupActivity;

        import static java.lang.Math.*;

public class BackgroundService extends Service{
    String logTag="BackgroundService";

    String CSVHeaer = "DATAPOINT,ELAPSED_TIME,TOTAL_REVOLUTIONS,INST_SPEED(RPM),INST_CADENCE(RPM),SPEED_BATTERY,CADENCE_BATTERY,POWER,POWER_BATTERY," +
            "USR,USR_BATTERY,HEART_RATE,GRADIENT,TEMPERATURE,HUB_BATTERY,INST_SPEED(KMH),INST_CADENCE(KMH),TIME_IN_SEC,AV_SPEED,AV_CADENCE,ADVANTAGE_IN_SEC,GEAR_RATIO,TOTAL_DISTANCE,AV_HEART_RATE,AV_POWER";

    //dummy
    public static String allLine = "6,00:10,138,38702,46,151,133,896,80,4,117,200,-50,176,82,4960,1473,10,3700,1098,2456,23:19351,867,135,877";

    private int oldAverageSpeed = 0;
    private int oldAverageCadence = 0;
    private int oldAvHeartRate = 0;
    private int oldAvPower = 0;

    private boolean firstLogRun = true;
    private int residueRev = 0;

            //(CSC_NOTIFIED,"CSC,"+totalDistance + "," + speed+","  +cadence
            //+ ","  + speedBattery+ ","  +  cadenceBattery);//stringBuilder.toString()
            //intent.putExtra(CSC_NOTIFIED, "RSC," + power + "," + powerBattery + "," + USR
            //+ "," + USRBattery);//stringBuilder.toString()
            //intent.putExtra(HR_NOTIFIED, "HR," + heartRate + "," + gradient + "," + tenmperature);

    private Handler ReadHandler;
    private ReadDataRunnable ReadDataRunnable;
    private static final int characteristicsPollingTimeInMillis = 1000;
    private boolean mConnected = false;

    private ArrayList<ArrayList<BluetoothGattCharacteristic>> mGattCharacteristics =
            new ArrayList<ArrayList<BluetoothGattCharacteristic>>();

    private ArrayList<BluetoothGattCharacteristic> mGattUSEDCharacteristics =
            new ArrayList<BluetoothGattCharacteristic>();

    private ArrayList<String> usedChars =
            new ArrayList<String>(Arrays.asList("CSC Measurement", "RSC Measurement", "Heart Rate Measurement"));
            //"Battery Level"

    private final String LIST_NAME = "NAME";
    private final String LIST_UUID = "UUID";

    private CSVWriter writer;


    private ArrayList[] dataBuffer = new ArrayList[4];

    private int receivedFlag = 0;  //         000...00000000 (empty mask)
    static final int CSCFlag = 1;    // 2^^0    000...00000001
    static final int RSCFlag = 2;    // 2^^1    000...00000010
    static final int HRFlag = 4;    // 2^^2    000...00000100
    static final int BLFlag = 8;    // 2^^3    000...00001000
    static final int allReveivedFlag = 15;    // 2^^3    000...00001111
    static final int allReveivedFlagALT = 7;    // 2^^3    000...00000111

    public static boolean recordStarted = false;
    public static boolean timerStarted = false;

    public long startTime;
    public static long startPauseTime = 0;
    public static long endPauseTime = 0;
    public static long PauseTimeDelta = 0;
    public int dataMeasurement = 1;

    private static IntentFilter makeGattUpdateIntentFilter() {
        final IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(ACTION_GATT_CONNECTED);
        intentFilter.addAction(ACTION_GATT_DISCONNECTED);
        intentFilter.addAction(ACTION_GATT_SERVICES_DISCOVERED);
        intentFilter.addAction(ACTION_DATA_AVAILABLE);
        intentFilter.addAction(CSC_NOTIFIED);
        intentFilter.addAction(RSC_NOTIFIED);
        intentFilter.addAction(HR_NOTIFIED);
        intentFilter.addAction(BL_NOTIFIED);
        intentFilter.addAction(ALL_RECEIVED);
        return intentFilter;
    }

    private final BroadcastReceiver mGattUpdateReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            final String action = intent.getAction();
            if (ACTION_GATT_CONNECTED.equals(action)) {
                mConnected = true;
            } else if (ACTION_GATT_DISCONNECTED.equals(action)) {
                mConnected = false;
                ReadHandler.removeCallbacks(ReadDataRunnable);

            } else if (ACTION_GATT_SERVICES_DISCOVERED.equals(action)) {
                // Show all the supported services and characteristics on the user interface.
                // and take out important ones
                onGattServicesConnected(getSupportedGattServices());
            } else if (ACTION_DATA_AVAILABLE.equals(action)) {
            }  else if (CSC_NOTIFIED.equals(action)) {
                dataBuffer[0].add(intent.getStringExtra(CSC_NOTIFIED));
            } else if (RSC_NOTIFIED.equals(action)) {
                dataBuffer[1].add(intent.getStringExtra(RSC_NOTIFIED));
            } else if (HR_NOTIFIED.equals(action)) {
                dataBuffer[2].add(intent.getStringExtra(HR_NOTIFIED));
            } else if (BL_NOTIFIED.equals(action)) {
                dataBuffer[3].add(intent.getStringExtra(BL_NOTIFIED));
            } else if (ALL_RECEIVED.equals(action)) {
                if (!recordStarted){
                    broadcastBattery();
                } else {
                    logData();
                }
            }

        }
    };




    @Override
    public IBinder onBind(Intent intent){
        return null;
    }
    @Override
    public void onCreate(){

        recordStarted = false;
        timerStarted = false;
        ReadHandler = new Handler();
        ReadDataRunnable =  new ReadDataRunnable();
        registerReceiver(mGattUpdateReceiver, makeGattUpdateIntentFilter());
        try {
            writer = new CSVWriter(new FileWriter("/sdcard/myfile.csv"), ',');
            String[] header = CSVHeaer.split(","); // array of your values
            writer.writeNext(header);
            writer.close();
        }
        catch (IOException e)
        {
            //error
        }

        dataBuffer[0] = new ArrayList();
        dataBuffer[1] = new ArrayList();
        dataBuffer[2] = new ArrayList();
        dataBuffer[3] = new ArrayList();

        super.onCreate();
    }

    private class ReadDataRunnable implements Runnable {
        @Override
        public void run() {
            getData();
            if (ReadHandler != null) {
                ReadHandler.postDelayed(this, characteristicsPollingTimeInMillis);
            }
        }
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId){
        if (intent !=null && intent.getExtras()!=null){

            String mDeviceAddress  = intent.getExtras().getString("bleAddress");

            if (!initialize()) {
                Log.e(logTag, "Unable to initialize Bluetooth");
                onDestroy();
            }
            else {
                if(!connect(mDeviceAddress)) {
                    Toast.makeText(this, "CONNECT FAILED", Toast.LENGTH_SHORT).show();
                }
                else {

                    //ReadHandler.post(ReadDataRunnable);
                }
            }

        }
        else {
            Log.e("SMARTBIKERR","SERVICE INTENT MISSING DATA");
        }
        //return START_STICKY;
        return START_NOT_STICKY;
    }

    @Override
    public void onDestroy(){
        close();
        ReadHandler.removeCallbacks(ReadDataRunnable);
        //mBluetoothGatt.close();
        super.onDestroy();
    }

    public void getData(){
        int i = 0;

        int fix =1;
        if(fix==1){
        while (i < mGattUSEDCharacteristics.size()) {
            BluetoothGattCharacteristic current =  mGattUSEDCharacteristics.get(i);

            setCharacteristicNotification(
                    current, true);
            readCharacteristic(current);
            try{ Thread.sleep(200); }catch(InterruptedException e){ }
            //mBluetoothLeService.setCharacteristicNotification(
            //		current, false);
            //Log.d("SMARTBIKERA", "UUID: " + current.getUuid().toString());
            i++;
            //logData();
        }
        }else {
            Log.e("SMARTER", "FORCE READ TRIAL");
            BluetoothGattCharacteristic csc = new BluetoothGattCharacteristic(UUID.fromString("00002a5b-0000-1000-8000-00805f9b34fb"), 16,0);
            BluetoothGattCharacteristic rsc = new BluetoothGattCharacteristic(UUID.fromString("00002a53-0000-1000-8000-00805f9b34fb"), 16,0);
            BluetoothGattCharacteristic hr = new BluetoothGattCharacteristic(UUID.fromString("00002a37-0000-1000-8000-00805f9b34fb"), 16,0);
            setCharacteristicNotification(csc, true);
            readCharacteristic(csc);
            try{ Thread.sleep(200); }catch(InterruptedException e){ }
            setCharacteristicNotification(rsc, true);
            readCharacteristic(rsc);
            try{ Thread.sleep(200); }catch(InterruptedException e){ }
            setCharacteristicNotification(hr, true);
            readCharacteristic(hr);
        }

        //Log.e("SMARTBIKERR", "Reading...");
    }

    public void broadcastBattery() {
        //String CSVHeaer = "DATAPOINT,ELAPSED_TIME,TOTAL_DISTANCE,INST_SPEED(RPM),INST_CADENCE(RPM),SPEED_BATTERY,CADENCE_BATTERY,POWER,POWER_BATTERY," +
        //        "USR,USR_BATTERY,HEART_RATE,GRADIENT,TEMPERATURE,HUB_BATTERY,INST_SPEED(KMH),INST_CADENCE(KMH),TIME_IN_SEC,AV_SPEED,AV_CADENCE,ADVANTAGE_IN_SEC,GEAR_RATIO";

        if (!recordStarted) {
            String CSCline = dataBuffer[0].get(0).toString();
            String RSCline = dataBuffer[1].get(0).toString();
            String HRline = dataBuffer[2].get(0).toString();
            String BLline = dataBuffer[3].get(0).toString();

            dataBuffer[0].clear();
            dataBuffer[1].clear();
            dataBuffer[2].clear();
            dataBuffer[3].clear();

            String[] CSClist = CSCline.split(",");
            String[] RSClist = RSCline.split(",");
            String[] HRlist = HRline.split(",");
            String[] BLlist = BLline.split(",");

            String hubBattery = BLlist[0];
            String wheelBattery = CSClist[3];
            String cadenceBattery = CSClist[4];
            String powerBattery = RSClist[1];
            String USRBattery = RSClist[3];

            String batteries = hubBattery + "," +  wheelBattery + "," + cadenceBattery + ","
                    + powerBattery + "," + USRBattery;

            Intent batteryBroadcast = new Intent(BATTERY_BROADCAST);
            batteryBroadcast.putExtra(BATTERY_BROADCAST,batteries);//stringBuilder.toString()
            sendBroadcast(batteryBroadcast);

        }
    }


    public void logData() {

        //if timer not started yet
        if (!timerStarted){
            timerStarted=true;
            startTime  = System.currentTimeMillis();
        }
        //String date = (DateFormat.format("dd-MM-yyyy hh:mm:ss", new java.util.Date()).toString());
        //String time = (DateFormat.format("mm:ss", new java.util.Date()).toString());
        long now = System.currentTimeMillis();
        long difference = now - startTime-(PauseTimeDelta);
        long differenceInSeconds = difference / DateUtils.SECOND_IN_MILLIS;
        // formatted will be HH:MM:SS or MM:SS
        String formattedTimeDifference = DateUtils.formatElapsedTime(differenceInSeconds);

        String CSCline = dataBuffer[0].get(0).toString();
        String RSCline = dataBuffer[1].get(0).toString();
        String HRline = dataBuffer[2].get(0).toString();
        String BLline = dataBuffer[3].get(0).toString();
        dataBuffer[0].clear();
        dataBuffer[1].clear();
        dataBuffer[2].clear();
        dataBuffer[3].clear();

        String[] CSClist = CSCline.split(",");
        String[] RSClist = RSCline.split(",");
        String[] HRlist = HRline.split(",");
        String[] BLlist = BLline.split(",");


        String wheelRPM = CSClist[1];
        String totalRev = CSClist[0];
        String speedBattery = CSClist[3];
        String pedalRPM = CSClist[2];
        String cadenceBattery = CSClist[4];
        String power = RSClist[0];
        String powerBattery = RSClist[1];
        String USR = RSClist[2];
        String USRBattery = RSClist[3];
        String gradient = HRlist[0];
        String tenmperature = HRlist[1];
        String heartRate = HRlist[2];
        String hubBattery = BLlist[0];

        /*
        The RPM to Linear Velocity formular is :
        v = r × RPM × 0.10472 ()
        Where:
        v: Linear velocity, in m/s
        r: Radius, in meter
        RPM: Angular velocity, in RPM (Rounds per Minute)

        */

        int realPower = (int)((Integer.valueOf(power)*50.0*SetupActivity.savedWheelRadius)/1000.0);

        int realSpeed = (int)(SetupActivity.savedWheelRadius * Math.PI * 2 * Integer.valueOf(wheelRPM)*60/1000000);//RPM * 60 *circum(km)

        //new_av = ((old_av * (num_samples - 1)) + latest_sample) / num_samples
        //int avSpeed = ((oldAverageSpeed* (dataMeasurement)) + realSpeed)/(dataMeasurement+1);

       // oldAverageSpeed = avSpeed;

        int realCadence =Integer.valueOf(pedalRPM); //(int)(SetupActivity.savedPedalRadius* Integer.valueOf(wheelRPM)* 0.10472 *3.6 / 1000);

        //new_av = ((old_av * (num_samples - 1)) + latest_sample) / num_samples
        int avCadence = ((oldAverageCadence* (dataMeasurement)) + realCadence)/(dataMeasurement+1);

        oldAverageCadence = avCadence;

        int avHeartRate =  ((oldAvHeartRate* (dataMeasurement)) + Integer.valueOf(heartRate))/(dataMeasurement+1);

        oldAvHeartRate = avHeartRate;

        int avPower = ((oldAvPower* (dataMeasurement)) + realPower)/(dataMeasurement+1);

        oldAvPower = avPower;

        int wheelRPMValue = Integer.valueOf(wheelRPM);
        int pedalRPMValue = Integer.valueOf(pedalRPM);

        int commonDivisor = gcdThing(pedalRPMValue, wheelRPMValue);

        if (commonDivisor!=0) {
            wheelRPMValue = wheelRPMValue / commonDivisor;

            pedalRPMValue = pedalRPMValue / commonDivisor;
        }else {
            wheelRPMValue = 0;
            pedalRPMValue = 0;
        }

        String gearRatio = String.valueOf(pedalRPMValue) + ":" + String.valueOf(wheelRPMValue);

        int totalRevVal = Integer.valueOf(totalRev);
        if (firstLogRun){
            residueRev = totalRevVal;
            firstLogRun = false;
        }
        int totalDistance = (int)((totalRevVal-residueRev) * (2.0 * PI*(SetupActivity.savedWheelRadius/1000.0)));

        int avSpeed = 0;
        if (differenceInSeconds !=0) {
            avSpeed = (int) ((totalDistance / differenceInSeconds) * 3.6);
        }

        //t_advantage = (v_av_actual - v_av_selected) * t / v_av_selected
        long advantageInTimeSec = ((avSpeed-SetupActivity.TARGET_AVERAGE_SPEED) * differenceInSeconds) / SetupActivity.TARGET_AVERAGE_SPEED;




                allLine = String.valueOf(dataMeasurement) + "," + formattedTimeDifference + "," + CSCline + "," + RSCline + "," + HRline + "," + BLline
        + "," + String.valueOf(realSpeed)+","+ String.valueOf(realCadence) +  "," + String.valueOf(differenceInSeconds)+","
                + String.valueOf(avSpeed) +"," + String.valueOf(avCadence) + "," + String.valueOf(advantageInTimeSec) + "," + gearRatio
                + "," + String.valueOf(totalDistance) + "," + String.valueOf(avHeartRate) + "," + String.valueOf(avPower);

        dataMeasurement++;

        Log.e(logTag, allLine);
            if (writer == null) {
                Log.e(logTag, "CSV Writer not initialised!");
            } else {
                try {
                    writer = new CSVWriter(new FileWriter("/sdcard/myfile.csv",true), ',');
                        //String[] line = (dataBuffer[i].get(index)).split(",");
                    writer.writeNext(allLine.split(","));
                    writer.close();
                    }

                 catch (IOException e)
                 {
                    //error
                }

            }
        //String CSVHeaer = "TIMESTAMP,TOTAL_DISTANCE,INST_SPEED,INST_CADENCE,SPEED_BATTERY,CADENCE_BATTERY,POWER,POWER_BATTERY,USR,USR_BATTERY,HEART_RATE,GRADIENT,TEMPERATURE,HUB_BATTERY";
        Log.e("SMARTBIKERR",allLine);

        Intent dataToActivity = new Intent(DATA_TO_ACTIVITY);
        dataToActivity.putExtra(DATA_TO_ACTIVITY,allLine);//stringBuilder.toString()
        sendBroadcast(dataToActivity);

        }

    public static void pauseLogging(){

        recordStarted = false;
        startPauseTime = System.currentTimeMillis();

    }

    public static void continueLogging(){
        endPauseTime  = System.currentTimeMillis();
        PauseTimeDelta = PauseTimeDelta + (endPauseTime- startPauseTime);
        recordStarted = true;
    }

    private static int gcdThing(int a, int b) {
        BigInteger b1 = new BigInteger(""+a); // there's a better way to do this. I forget.
        BigInteger b2 = new BigInteger(""+b);
        BigInteger gcd = b1.gcd(b2);
        return gcd.intValue();
    }



    private void onGattServicesConnected(List<BluetoothGattService> gattServices) {
        if (gattServices == null) {
            return;
        }

        String uuid = null;
        String unknownServiceString = getResources().getString(R.string.unknown_service);
        String unknownCharaString = getResources().getString(R.string.unknown_characteristic);
        ArrayList<HashMap<String, String>> gattServiceData = new ArrayList<HashMap<String, String>>();
        ArrayList<ArrayList<HashMap<String, String>>> gattCharacteristicData
                = new ArrayList<ArrayList<HashMap<String, String>>>();
        mGattCharacteristics = new ArrayList<ArrayList<BluetoothGattCharacteristic>>();
        mGattUSEDCharacteristics = new ArrayList<BluetoothGattCharacteristic>();
        // Loops through available GATT Services.
        for (BluetoothGattService gattService : gattServices) {
            HashMap<String, String> currentServiceData = new HashMap<String, String>();
            currentServiceData.put(
                    LIST_NAME, com.movisens.smartgattlib.Service.lookup(gattService.getUuid(), unknownServiceString));
            currentServiceData.put(LIST_UUID, uuid);
            gattServiceData.add(currentServiceData);

            ArrayList<HashMap<String, String>> gattCharacteristicGroupData =
                    new ArrayList<HashMap<String, String>>();
            List<BluetoothGattCharacteristic> gattCharacteristics =
                    gattService.getCharacteristics();
            ArrayList<BluetoothGattCharacteristic> charas =
                    new ArrayList<BluetoothGattCharacteristic>();

            // Loops through available Characteristics.
            for (BluetoothGattCharacteristic gattCharacteristic : gattCharacteristics) {
                charas.add(gattCharacteristic);
                HashMap<String, String> currentCharaData = new HashMap<String, String>();
                uuid = gattCharacteristic.getUuid().toString();
                if(uuid.contains("00002a5b")){
                    Log.e("SMARTER", "CSC PRESENT");
                    int perm = gattCharacteristic.getPermissions();
                    int prop = gattCharacteristic.getProperties();
                    Log.e("SMARTER", "CSC PERM: "+ perm + ",PROP: " +prop);

                } else if (uuid.contains("00002a53")){
                    Log.e("SMARTER","RSC PRESENT");
                    int perm = gattCharacteristic.getPermissions();
                    int prop = gattCharacteristic.getProperties();
                    Log.e("SMARTER", "RSC PERM: "+ perm + ",PROP: " +prop);
                } else if (uuid.contains("00002a37")) {
                    Log.e("SMARTER", "HR PRESENT");
                    int perm = gattCharacteristic.getPermissions();
                    int prop = gattCharacteristic.getProperties();
                    Log.e("SMARTER", "HR PERM: "+ perm + ",PROP: " +prop);
                }

                String key = LIST_NAME;
                String value = Characteristic.lookup(gattCharacteristic.getUuid(), unknownCharaString);
                //Log.e("SMARTER",value + "," +  uuid);
                int i = 0;
                while (i < usedChars.size()) {
                    if (value.equals(usedChars.get(i))){
                        mGattUSEDCharacteristics.add(gattCharacteristic);
                        //mBluetoothLeService.setCharacteristicNotification(
                        //		gattCharacteristic, true);
                    }
                    i++;
                }



                //mBluetoothLeService.readCharacteristic(gattCharacteristic);
                //try{ Thread.sleep(100); }catch(InterruptedException e){ }
                //Log.d("SMARTBIKERA", "NAME: " + value + ", UUID: " + uuid);

                currentCharaData.put(key, value);
                currentCharaData.put(LIST_UUID, uuid);
                gattCharacteristicGroupData.add(currentCharaData);
            }
            mGattCharacteristics.add(charas);
            gattCharacteristicData.add(gattCharacteristicGroupData);
        }

        ReadHandler.post(ReadDataRunnable);
    }



    private final static String TAG = "BackgroundService";
    //.class.getSimpleName();
    protected static final UUID CHARACTERISTIC_UPDATE_NOTIFICATION_DESCRIPTOR_UUID = UUID.fromString("00002902-0000-1000-8000-00805f9b34fb");
    //protected static final UUID CHARACTERISTIC_UPDATE_NOTIFICATION_DESCRIPTOR_UUID = UUID.fromString("00002a37-0000-1000-8000-00805f9b34fb");
    private static final UUID RSC_MEASUREMENT_CHARACTERISTIC_UUID = UUID.fromString("00002A53-0000-1000-8000-00805f9b34fb");

    //RSC descriptor
    private static final UUID CLIENT_CHARACTERISTIC_CONFIG_DESCRIPTOR_UUID = UUID.fromString("00002902-0000-1000-8000-00805f9b34fb");


    private BluetoothManager mBluetoothManager;
    private BluetoothAdapter mBluetoothAdapter;
    private String mBluetoothDeviceAddress;
    private BluetoothGatt mBluetoothGatt;
    private int mConnectionState = STATE_DISCONNECTED;

    private static final int STATE_DISCONNECTED = 0;
    private static final int STATE_CONNECTING = 1;
    private static final int STATE_CONNECTED = 2;

    public final static String ACTION_GATT_CONNECTED =
            "com.example.bluetooth.le.ACTION_GATT_CONNECTED";
    public final static String ACTION_GATT_DISCONNECTED =
            "com.example.bluetooth.le.ACTION_GATT_DISCONNECTED";
    public final static String ACTION_GATT_SERVICES_DISCOVERED =
            "com.example.bluetooth.le.ACTION_GATT_SERVICES_DISCOVERED";
    public final static String ACTION_DATA_AVAILABLE =
            "com.example.bluetooth.le.ACTION_DATA_AVAILABLE";
    public final static String EXTRA_DATA =
            "com.example.bluetooth.le.EXTRA_DATA";

    public final static String CSC_NOTIFIED =
            "lukacstomi91.smartbike.CSC_NOTIFIED";
    public final static String RSC_NOTIFIED =
            "lukacstomi91.smartbike.RSC_NOTIFIED";
    public final static String HR_NOTIFIED =
            "lukacstomi91.smartbike.HR_NOTIFIED";
    public final static String BL_NOTIFIED =
            "lukacstomi91.smartbike.BL_NOTIFIED";
    public final static String ALL_RECEIVED =
            "lukacstomi91.smartbike.ALL_RECEIVED";
    public final static String DATA_TO_ACTIVITY =
            "lukacstomi91.smartbike.DATA_TO_ACTIVITY";
    public final static String BATTERY_BROADCAST =
            "lukacstomi91.smartbike.BATTERY_BROADCAST";


    // Implements callback methods for GATT events that the app cares about.  For example,
    // connection change and services discovered.
    private final BluetoothGattCallback mGattCallback = new BluetoothGattCallback() {
        @Override
        public void onConnectionStateChange(BluetoothGatt gatt, int status, int newState) {
            String intentAction;
            if (newState == BluetoothProfile.STATE_CONNECTED) {
                intentAction = ACTION_GATT_CONNECTED;
                mConnectionState = STATE_CONNECTED;
                broadcastUpdate(intentAction);
                Log.i(TAG, "Connected to GATT server.");
                // Attempts to discover services after successful connection.
                Log.i(TAG, "Attempting to start service discovery:" +
                        mBluetoothGatt.discoverServices());

            } else if (newState == BluetoothProfile.STATE_DISCONNECTED) {
                intentAction = ACTION_GATT_DISCONNECTED;
                mConnectionState = STATE_DISCONNECTED;
                Log.i(TAG, "Disconnected from GATT server.");
                broadcastUpdate(intentAction);
            }
        }

        @Override
        public void onServicesDiscovered(BluetoothGatt gatt, int status) {
            if (status == BluetoothGatt.GATT_SUCCESS) {
                broadcastUpdate(ACTION_GATT_SERVICES_DISCOVERED);
                //READ CHARACTERISTICS
                //onGattServicesConnected(getSupportedGattServices());
            } else {
                Log.w(TAG, "onServicesDiscovered received: " + status);
            }
        }

        @Override
        public void onCharacteristicRead(BluetoothGatt gatt,
                                         BluetoothGattCharacteristic characteristic,
                                         int status) {
            UUID characteristicUuid = characteristic.getUuid();

            String serviceName = Characteristic.lookup(characteristicUuid, "unknown");
            //Log.e("SMARTBIKERR","onCharacteristicRead: " + serviceName);

            if (status == BluetoothGatt.GATT_SUCCESS) {

                broadcastUpdate(ACTION_DATA_AVAILABLE, characteristic);
            }
        }

        @Override
        public void onCharacteristicChanged(BluetoothGatt gatt,
                                            BluetoothGattCharacteristic characteristic) {
            UUID characteristicUuid = characteristic.getUuid();

            String serviceName = Characteristic.lookup(characteristicUuid, "unknown");
            //Log.e("SMARTBIKERR","onCharacteristicChanged: " + serviceName);
            broadcastUpdate(ACTION_DATA_AVAILABLE, characteristic);
        }
    };

    private void broadcastUpdate(final String action) {
        Log.e("SMARTBIKERR","action?: " + action);
        final Intent intent = new Intent(action);
        sendBroadcast(intent);
    }

    private void broadcastUpdate(final String action,
                                 BluetoothGattCharacteristic characteristic) {



        /* DEBUG CHECK - ALL SERVICE IS THERE

        List<BluetoothGattService> loller = new ArrayList<BluetoothGattService>();
        loller = getSupportedGattServices();

        int i = 0;
        while (i < loller.size()) {
            List<BluetoothGattCharacteristic> inst = loller.get(i).getCharacteristics();
            int b = 0;
            while (b < inst.size()) {

                String val = Characteristic.lookup(inst.get(b).getUuid(), "unknown");
                Log.e("SMARTBIKERR", "support: " + val);
                b++;
            }
            i++;
        }
        */
        // This is special handling for the Heart Rate Measurement profile.  Data parsing is
        // carried out by the SmartGattLib
        UUID characteristicUuid = characteristic.getUuid();

        String serviceName = Characteristic.lookup(characteristicUuid, "unknown");
        //Log.e("SMARTBIKERR", "UUID: " + serviceName);
        //final byte[] data = characteristic.getValue();

        //String nam = Characteristic.lookup(characteristicUuid, "unknown");


        //Log.e("SMARTBIKERR","Name: " + nam +" ,ID: " + characteristicUuid + " ,DATA: " + data);

        /*
        DATA        VALUES  UNITS                   FORMAT      CHARACTERISTIC

        Speed	    0-500   (speed in km/h * 10)	uint16	    cscs measurement, last wheel event time
        Total dist	0-65000 (m)	                    uint32	    cscs measurement, cumulative wheel revs
        Speed Bat	0-100	                        uint8(16)	(cscs measurement, last crank event time) upper byte
        Cadence	    0-200	                        uint8	    cscs measurement, cumulative crank revs
        Cadence bat	0-100	                        uint8(16)	(cscs measurement, last crank event time) lower byte
        Power	    0-1000	                        uint16	    rcrs measurement, inst speed
        Power bat	0-100	                        uint8	    rcrs measurement, inst cadence
        USR	        0-250 (meters*10)	            uint16	    rcrs measurement, inst stride length
        USR bat	    0-100	                        uint8(32)	rcrs measurement, total distance
        Gradient	0-100 (degrees +50)	            uint8	    (hrs measurment, RR-interval) upper byte
        Temp	    0-120 (degrees +50)	            uint8	    (hrs measurment, RR-interval) lower byte
        Heart Rate	0-250	                        uint8   	hrs measurment, heart rate measurement value
        Hub Bat	    0-100	                        uint8	    Battery service

        */

        int offset = 0;

        int speed = -99;
        int totalDistance = -99;
        int speedBattery = -99;
        int cadence = -99;
        int cadenceBattery = -99;
        int power = -99;
        int powerBattery = -99;
        int USR = -99;
        int USRBattery = -99;
        int gradient = -99;
        int tenmperature = -99;
        int heartRate = -99;
        int hubBattery = -99;


        if (Characteristic.CSC_MEASUREMENT.equals(characteristicUuid)) { // Identify


            //TOTAL DIST = cumulative wheel revs (uint32)
            offset = 1;
            totalDistance = characteristic.getIntValue(BluetoothGattCharacteristic.FORMAT_UINT32, offset);

            //SPEED = lastWheelEventTime(uint16)
            offset = 5;
            speed = characteristic.getIntValue(BluetoothGattCharacteristic.FORMAT_UINT16, offset);

            //CADENCE = Cumulative Crank Revolutionsuint(uint16)
            offset = 7;
            cadence = characteristic.getIntValue(BluetoothGattCharacteristic.FORMAT_UINT8, offset);
            ;
            //Speed Bat + Cadence bat = Last Crank Event Time
            offset = 9;
            speedBattery = characteristic.getIntValue(BluetoothGattCharacteristic.FORMAT_UINT8, offset);

            offset = 10;
            cadenceBattery = characteristic.getIntValue(BluetoothGattCharacteristic.FORMAT_UINT8, offset);

            /*
            Log.e("SMARTBIKERR", "Property: " + "totalDistance" + " ,DATA: " + totalDistance);
            Log.e("SMARTBIKERR", "Property: " + "speed" + " ,DATA: " + speed);
            Log.e("SMARTBIKERR", "Property: " + "cadence" + " ,DATA: " + cadence);
            Log.e("SMARTBIKERR", "Property: " + "speedBattery" + " ,DATA: " + speedBattery);
            Log.e("SMARTBIKERR", "Property: " + "cadenceBattery" + " ,DATA: " + cadenceBattery);
            */
            receivedFlag |= CSCFlag;


            final Intent intent = new Intent(CSC_NOTIFIED);

            intent.putExtra(CSC_NOTIFIED, totalDistance + "," + speed + "," + cadence
                    + "," + speedBattery + "," + cadenceBattery);//stringBuilder.toString()
            sendBroadcast(intent);

        } else if (Characteristic.RSC_MEASUREMENT.equals(characteristicUuid)) { // Identify

            //POWER = inst speed (uint16)
            offset = 1;
            power = characteristic.getIntValue(BluetoothGattCharacteristic.FORMAT_UINT16, offset);

            //POWER BATTERY = inst cadence(uint8)
            offset = 3;
            powerBattery = characteristic.getIntValue(BluetoothGattCharacteristic.FORMAT_UINT8, offset);

            //USR = inst stride length(uint16)
            offset = 4;
            USR = characteristic.getIntValue(BluetoothGattCharacteristic.FORMAT_UINT16, offset) /10;//scaling

            //USR bat = total distance(uint32)
            offset = 6;
            USRBattery = characteristic.getIntValue(BluetoothGattCharacteristic.FORMAT_UINT8, offset);


            receivedFlag |= RSCFlag;
            //ALT!!!!!
            //USR bat = total distance(uint32)
            offset = 7;
            hubBattery = characteristic.getIntValue(BluetoothGattCharacteristic.FORMAT_UINT8, offset);

            receivedFlag |= BLFlag;
            /*
            Log.e("SMARTBIKERR", "Property: " + "hubBattery" + " ,DATA: " + (hubBattery));
            */
            final Intent intent = new Intent(BL_NOTIFIED);
            intent.putExtra(BL_NOTIFIED, String.valueOf(hubBattery));//stringBuilder.toString()
            sendBroadcast(intent);




            /*
            Log.e("SMARTBIKERR", "Property: " + "power" + " ,DATA: " + power);
            Log.e("SMARTBIKERR", "Property: " + "powerBattery" + " ,DATA: " + powerBattery);
            Log.e("SMARTBIKERR", "Property: " + "USR" + " ,DATA: " + USR);
            Log.e("SMARTBIKERR", "Property: " + "USRBattery" + " ,DATA: " + USRBattery);
            */
            final Intent intent2 = new Intent(RSC_NOTIFIED);
            intent2.putExtra(RSC_NOTIFIED, power + "," + powerBattery + "," + USR
                    + "," + USRBattery);//stringBuilder.toString()
            sendBroadcast(intent2);

        } else if (Characteristic.HEART_RATE_MEASUREMENT.equals(characteristicUuid)) {

            //Heart Rate = heart rate measurement value(uint8)
            offset = 1;
            heartRate = characteristic.getIntValue(BluetoothGattCharacteristic.FORMAT_UINT8, offset);

            //Gradient = RR-interval (uint16)upper
            offset = 7;
            gradient = characteristic.getIntValue(BluetoothGattCharacteristic.FORMAT_UINT8, offset) -50; //offset

            //
            offset = 6;
            tenmperature = characteristic.getIntValue(BluetoothGattCharacteristic.FORMAT_UINT8, offset)-50;

            receivedFlag |= HRFlag;
            /*
            Log.e("SMARTBIKERR", "Property: " + "heartRate" + " ,DATA: " + heartRate);
            Log.e("SMARTBIKERR", "Property: " + "gradient" + " ,DATA: " + gradient);
            Log.e("SMARTBIKERR", "Property: " + "tenmperature" + " ,DATA: " + (tenmperature));
            */
            final Intent intent = new Intent(HR_NOTIFIED);
            intent.putExtra(HR_NOTIFIED, heartRate + "," + gradient + "," + tenmperature);//stringBuilder.toString()
            sendBroadcast(intent);


        } else if (Characteristic.BATTERY_LEVEL.equals(characteristicUuid)) {

            // Hub Bat = Battery service(uint8)
            offset = 0;
            hubBattery = characteristic.getIntValue(BluetoothGattCharacteristic.FORMAT_UINT8, offset);

            //receivedFlag |= BLFlag;
            /*
            Log.e("SMARTBIKERR", "Property: " + "hubBattery" + " ,DATA: " + (hubBattery));
            */
            /*
            final Intent intent = new Intent(BL_NOTIFIED);
            intent.putExtra(BL_NOTIFIED, String.valueOf(hubBattery));//stringBuilder.toString()
            sendBroadcast(intent);
            */



            /*
            BatteryLevel bl = new BatteryLevel(data);

            hubBattery = bl.getBatteryLevel();
            intent.putExtra(EXTRA_DATA, "BatteryLevel: " + hubBattery
                    + "%");

            Log.e("SMARTBIKERR", "Property: " + "hubBattery" + " ,DATA: " + (hubBattery));


        } else if (Characteristic.HEART_RATE_MEASUREMENT.equals(characteristicUuid)) { // Identify
            // Characteristic
            HeartRateMeasurement hrm = new HeartRateMeasurement(data); // Interpret
            // Characteristic
            intent.putExtra(EXTRA_DATA, "HR: " + hrm.getHr() + "bpm" + ", EE: "
                    + hrm.getEe() + "kJ" + ", Status: " + hrm.getSensorWorn());
        */

        } else {
            byte[] data = characteristic.getValue();

            Log.e("SMARTBIKERR", "ELSE ARRIVED");
            // For all other profiles, writes the data formatted in HEX.
            if (data != null && data.length > 0) {
                final StringBuilder stringBuilder = new StringBuilder(data.length);
                for (byte byteChar : data)
                    stringBuilder.append(String.format("%02X ", byteChar));
                //intent.putExtra(EXTRA_DATA, new String(data) + "\n" + stringBuilder.toString());
            }
        }

        if (receivedFlag == allReveivedFlag) {
            receivedFlag = 0;
            final Intent intentAllReveived = new Intent(ALL_RECEIVED);
            sendBroadcast(intentAllReveived);
            //sendBroadcast(intent);
        }
    }


    @Override
    public boolean onUnbind(Intent intent) {
        // After using a given device, you should make sure that BluetoothGatt.close() is called
        // such that resources are cleaned up properly.  In this particular example, close() is
        // invoked when the UI is disconnected from the Service.
        close();
        return super.onUnbind(intent);
    }


    /**
     * Initializes a reference to the local Bluetooth adapter.
     *
     * @return Return true if the initialization is successful.
     */
    public boolean initialize() {
        // For API level 18 and above, get a reference to BluetoothAdapter through
        // BluetoothManager.
        if (mBluetoothManager == null) {
            mBluetoothManager = (BluetoothManager) getSystemService(Context.BLUETOOTH_SERVICE);
            if (mBluetoothManager == null) {
                Log.e(TAG, "Unable to initialize BluetoothManager.");
                return false;
            }
        }

        mBluetoothAdapter = mBluetoothManager.getAdapter();
        if (mBluetoothAdapter == null) {
            Log.e(TAG, "Unable to obtain a BluetoothAdapter.");
            return false;
        }

        return true;
    }

    /**
     * Connects to the GATT server hosted on the Bluetooth LE device.
     *
     * @param address The device address of the destination device.
     *
     * @return Return true if the connection is initiated successfully. The connection result
     *         is reported asynchronously through the
     *         {@code BluetoothGattCallback#onConnectionStateChange(android.bluetooth.BluetoothGatt, int, int)}
     *         callback.
     */
    public boolean connect(final String address) {
        if (mBluetoothAdapter == null || address == null) {
            Log.w(TAG, "BluetoothAdapter not initialized or unspecified address.");
            return false;
        }

        // Previously connected device.  Try to reconnect.
        if (mBluetoothDeviceAddress != null && address.equals(mBluetoothDeviceAddress)
                && mBluetoothGatt != null) {
            Log.d(TAG, "Trying to use an existing mBluetoothGatt for connection.");
            if (mBluetoothGatt.connect()) {
                mConnectionState = STATE_CONNECTING;
                return true;
            } else {
                return false;
            }
        }

        final BluetoothDevice device = mBluetoothAdapter.getRemoteDevice(address);
        if (device == null) {
            Log.w(TAG, "Device not found.  Unable to connect.");
            return false;
        }
        // We want to directly connect to the device, so we are setting the autoConnect
        // parameter to false.
        mBluetoothGatt = device.connectGatt(this, false, mGattCallback);
        Log.d(TAG, "Trying to create a new connection.");
        mBluetoothDeviceAddress = address;
        mConnectionState = STATE_CONNECTING;
        return true;
    }

    /**
     * Disconnects an existing connection or cancel a pending connection. The disconnection result
     * is reported asynchronously through the
     * {@code BluetoothGattCallback#onConnectionStateChange(android.bluetooth.BluetoothGatt, int, int)}
     * callback.
     */
    public void disconnect() {
        if (mBluetoothAdapter == null || mBluetoothGatt == null) {
            Log.w(TAG, "BluetoothAdapter not initialized");
            return;
        }
        mBluetoothGatt.disconnect();
    }

    /**
     * After using a given BLE device, the app must call this method to ensure resources are
     * released properly.
     */
    public void close() {
        if (mBluetoothGatt == null) {
            return;
        }
        mBluetoothGatt.close();
        mBluetoothGatt = null;
    }

    /**
     * Request a read on a given {@code BluetoothGattCharacteristic}. The read result is reported
     * asynchronously through the {@code BluetoothGattCallback#onCharacteristicRead(android.bluetooth.BluetoothGatt, android.bluetooth.BluetoothGattCharacteristic, int)}
     * callback.
     *
     * @param characteristic The characteristic to read from.
     */
    public void readCharacteristic(BluetoothGattCharacteristic characteristic) {
        if (mBluetoothAdapter == null || mBluetoothGatt == null) {
            Log.w(TAG, "BluetoothAdapter not initialized");
            return;
        }
        mBluetoothGatt.readCharacteristic(characteristic);
    }

    /**
     * Enables or disables notification on a give characteristic.
     *
     * @param characteristic Characteristic to act on.
     * @param enabled If true, enable notification.  False otherwise.
     */
    public void setCharacteristicNotification(BluetoothGattCharacteristic characteristic,
                                              boolean enabled) {
        if (mBluetoothAdapter == null || mBluetoothGatt == null) {
            Log.w(TAG, "BluetoothAdapter not initialized");
            return;
        }
        mBluetoothGatt.setCharacteristicNotification(characteristic, enabled);

        if (Characteristic.CSC_MEASUREMENT.equals(characteristic.getUuid())) {
            BluetoothGattDescriptor descriptor = characteristic.getDescriptor(Descriptor.CLIENT_CHARACTERISTIC_CONFIGURATION);
            descriptor.setValue(enabled ? BluetoothGattDescriptor.ENABLE_NOTIFICATION_VALUE : BluetoothGattDescriptor.DISABLE_NOTIFICATION_VALUE);
            mBluetoothGatt.writeDescriptor(descriptor);
        }
        else if (Characteristic.RSC_MEASUREMENT.equals(characteristic.getUuid())) {
            final BluetoothGattDescriptor descriptor = characteristic.getDescriptor(CLIENT_CHARACTERISTIC_CONFIG_DESCRIPTOR_UUID);
            descriptor.setValue(BluetoothGattDescriptor.ENABLE_NOTIFICATION_VALUE);
            mBluetoothGatt.writeDescriptor(descriptor);
        }
        else if (Characteristic.HEART_RATE_MEASUREMENT.equals(characteristic.getUuid())) {
            BluetoothGattDescriptor descriptor = characteristic.getDescriptor(Descriptor.CLIENT_CHARACTERISTIC_CONFIGURATION);
            descriptor.setValue(enabled ? BluetoothGattDescriptor.ENABLE_NOTIFICATION_VALUE : BluetoothGattDescriptor.DISABLE_NOTIFICATION_VALUE);
            mBluetoothGatt.writeDescriptor(descriptor);
        }
        else if (Characteristic.BATTERY_LEVEL.equals(characteristic.getUuid())) {
            BluetoothGattDescriptor descriptor = characteristic.getDescriptor(Descriptor.CLIENT_CHARACTERISTIC_CONFIGURATION);
            descriptor.setValue(enabled ? BluetoothGattDescriptor.ENABLE_NOTIFICATION_VALUE : BluetoothGattDescriptor.DISABLE_NOTIFICATION_VALUE);
            mBluetoothGatt.writeDescriptor(descriptor);
        }


        //descriptor.setValue(enabled ? BluetoothGattDescriptor.ENABLE_NOTIFICATION_VALUE : new byte[]{0x00, 0x00});
        //ENABLE_INDICATION_VALUE

        //BluetoothGattDescriptor descriptor = characteristic.getDescriptor(CHARACTERISTIC_UPDATE_NOTIFICATION_DESCRIPTOR_UUID);
        //descriptor.setValue(enable ? BluetoothGattDescriptor.ENABLE_NOTIFICATION_VALUE : new byte[] { 0x00, 0x00 });

        /*

        // This is specific to Heart Rate Measurement.


        // This is specific to Heart Rate Measurement.


        // This is specific to Heart Rate Measurement.
        if (Characteristic.RSC_MEASUREMENT.equals(characteristic.getUuid())) {
            BluetoothGattDescriptor descriptor = characteristic.getDescriptor(Descriptor.CLIENT_CHARACTERISTIC_CONFIGURATION);
            descriptor.setValue(enabled ? BluetoothGattDescriptor.ENABLE_NOTIFICATION_VALUE : BluetoothGattDescriptor.DISABLE_NOTIFICATION_VALUE);
            mBluetoothGatt.writeDescriptor(descriptor);
        }

        if (Characteristic.BATTERY_LEVEL.equals(characteristic.getUuid())) {
            BluetoothGattDescriptor descriptor = characteristic.getDescriptor(Descriptor.CLIENT_CHARACTERISTIC_CONFIGURATION);
            descriptor.setValue(enabled ? BluetoothGattDescriptor.ENABLE_NOTIFICATION_VALUE : BluetoothGattDescriptor.DISABLE_NOTIFICATION_VALUE);
            mBluetoothGatt.writeDescriptor(descriptor);
        }

       */



    }

    /**
     * Retrieves a list of supported GATT services on the connected device. This should be
     * invoked only after {@code BluetoothGatt#discoverServices()} completes successfully.
     *
     * @return A {@code List} of supported services.
     */
    public List<BluetoothGattService> getSupportedGattServices() {
        if (mBluetoothGatt == null) return null;

        return mBluetoothGatt.getServices();
    }





}