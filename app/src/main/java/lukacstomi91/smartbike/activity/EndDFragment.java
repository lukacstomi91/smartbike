package lukacstomi91.smartbike.activity;

/**
 * Created by Tom on 2016. 05. 09..
 */
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Switch;

import butterknife.Bind;
import butterknife.ButterKnife;
import lukacstomi91.smartbike.R;
import lukacstomi91.smartbike.ble.BackgroundService;

public class EndDFragment extends DialogFragment {


    @Bind(R.id.endFragmentOKButton)
    Button endFragmentOKButton;

    @Bind(R.id.endFragmentCancelButton)
    Button endFragmentCancelButton;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_end, container,
                false);
        setCancelable(false);
        ButterKnife.bind(this, rootView);

        getDialog().setTitle("Ending Session");


        endFragmentCancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                BackgroundService.continueLogging();
                dismiss();
            }
        });

        endFragmentOKButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                BackgroundService.recordStarted = false;
                Intent intent = new Intent(getActivity(), EndActivity.class);
                startActivity(intent);
                dismiss();
            }
        });

        // Do something else
        return rootView;
    }
}