package lukacstomi91.smartbike.activity;

import android.content.Intent;
import android.content.res.Configuration;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Set;

import butterknife.Bind;
import butterknife.ButterKnife;
import lukacstomi91.smartbike.R;
import lukacstomi91.smartbike.ble.BackgroundService;
import lukacstomi91.smartbike.log.SimpleFileDialog;

/**
 * Created by Tom on 2016. 02. 19..
 */
public class EndActivity  extends AppCompatActivity
{
    public static final int SAVE_FILE_RESULT_CODE = 10;

    FragmentManager fm = getSupportFragmentManager();

    @Bind(R.id.endTwHeader)
    TextView endTwHeader;

    @Bind(R.id.endTwHeader2)
    TextView endTwHeader2;

    @Bind(R.id.endTwGoal)
    TextView endTwGoal;


    @Bind(R.id.endTwCadenceValue)
    TextView endTwCadenceValue;

    @Bind(R.id.endTwDistanceValue)
    TextView endTwDistanceValue;

    @Bind(R.id.endTwHeartValue)
    TextView endTwHeartValue;

    @Bind(R.id.endTwPowerValue)
    TextView endTwPowerValue;

    @Bind(R.id.endTwSpeedValue)
    TextView endTwSpeedValue;

    @Bind(R.id.endTwTimeValue)
    TextView endTwTimeValue;

    @Bind(R.id.endButtonExit)
    Button endButtonExit;

    @Bind(R.id.endButtonRestart)
    Button endButtonRestart;

    @Bind(R.id.endButtonExport)
    Button endButtonExport;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_end);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        ButterKnife.bind(this); // Binding layout elements

        parseLastResults(BackgroundService.allLine);

        getSupportActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
        getSupportActionBar().setCustomView(R.layout.alternative_actionbar);

        final TextView titleFix = (TextView) findViewById(R.id.titlebarText);
        titleFix.setText(R.string.end_title);

        endButtonExit.setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                ExitDFragment ExitDFragment = new ExitDFragment();
                ExitDFragment.show(fm, "Dialog Fragment");


            }
        });

        endButtonRestart.setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                Intent stopS = new Intent(EndActivity.this, BackgroundService.class);
                stopService(stopS);

                Intent intent = new Intent(EndActivity.this, MainActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);

            }
        });

        //Button2

        endButtonExport.setOnClickListener(new View.OnClickListener() {
            String m_chosen;

            @Override
            public void onClick(View v) {
                /////////////////////////////////////////////////////////////////////////////////////////////////
                //Create FileSaveDialog and register a callback
                /////////////////////////////////////////////////////////////////////////////////////////////////
                SimpleFileDialog FileSaveDialog = new SimpleFileDialog(EndActivity.this, "FileSave",
                        new SimpleFileDialog.SimpleFileDialogListener() {
                            @Override
                            public void onChosenDir(String chosenDir) {
                                // The code in this function will be executed when the dialog OK button is pushed
                                m_chosen = chosenDir;
                                Toast.makeText(EndActivity.this, "Chosen FileOpenDialog File: " +
                                        m_chosen, Toast.LENGTH_LONG).show();
                                moveFile("/storage/emulated/0/myfile.csv",m_chosen);


                            }
                        });

                //You can change the default filename using the public variable "Default_File_Name"
                String date = (DateFormat.format("dd_MM_yyyy hh_mm_ss", new java.util.Date()).toString());

                FileSaveDialog.Default_File_Name = date + "_session.csv";
                FileSaveDialog.chooseFile_or_Dir();

                /////////////////////////////////////////////////////////////////////////////////////////////////

            }
        });


    }

    private void moveFile(String inputPath, String outputPath) {
        Log.e("SMITE","IN: " + inputPath + ",OUT: " + outputPath);

        InputStream in = null;
        OutputStream out = null;
        try {
            /*
            //create output directory if it doesn't exist
            File dir = new File (outputPath);
            if (!dir.exists())
            {
                dir.mkdirs();
            }
            */


            in = new FileInputStream(inputPath);
            out = new FileOutputStream(outputPath);

            byte[] buffer = new byte[1024];
            int read;
            while ((read = in.read(buffer)) != -1) {
                out.write(buffer, 0, read);
            }
            in.close();
            in = null;

            // write the output file
            out.flush();
            out.close();
            out = null;

            // delete the original file
            new File(inputPath).delete();


        }

        catch (FileNotFoundException fnfe1) {
            Log.e("tag", fnfe1.getMessage());
        }
        catch (Exception e) {
            Log.e("tag", e.getMessage());
        }

    }

    public void parseLastResults(String list){
        Log.e("FUCKER", "LOL");
        String[] data = list.split(",");
        //String CSVHeaer = "DATAPOINT,ELAPSED_TIME,TOTAL_REVOLUTIONS,INST_SPEED(RPM),INST_CADENCE(RPM),SPEED_BATTERY,CADENCE_BATTERY,POWER,POWER_BATTERY," +
        //(9)"USR,USR_BATTERY,HEART_RATE,GRADIENT,TEMPERATURE,HUB_BATTERY,INST_SPEED(KMH),INST_CADENCE(KMH),TIME_IN_SEC,AV_SPEED,AV_CADENCE,(20)ADVANTAGE_IN_SEC,GEAR_RATIO,TOTAL_DISTANCE,(23)AV_HEART_RATE,AV_POWER";

        String avSpeed = data[18];
        String avCadence = data[19];
        String avPower = data[24];
        String avHeartRate = data[23];
        String distance = data[22];
        String elapsedTime = data[1];

        int avSpeedValue = Integer.valueOf(avSpeed);

        if (avSpeedValue>= SetupActivity.TARGET_AVERAGE_SPEED){
            //person won
            endTwHeader.setText(getResources().getString(R.string.end_win_header));
            endTwHeader2.setText(getResources().getString(R.string.end_win_header2));
            endTwGoal.setText("(Complete Session with "+String.valueOf(SetupActivity.TARGET_AVERAGE_SPEED) +" km/h average speed)");
        } else {
            endTwHeader.setText(getResources().getString(R.string.end_lost_header));
            endTwHeader2.setText(getResources().getString(R.string.end_lost_header2));
            endTwGoal.setText("(Complete Session with "+String.valueOf(SetupActivity.TARGET_AVERAGE_SPEED) +" km/h average speed)");
        }


        endTwCadenceValue.setText(avCadence);
        endTwSpeedValue.setText(avSpeed);
        endTwDistanceValue.setText(distance);
        endTwPowerValue.setText(avPower);
        endTwHeartValue.setText(avHeartRate);
        endTwTimeValue.setText(elapsedTime);


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // action with ID action_refresh was selected
            case R.id.action_debug:
                Toast.makeText(this, "Debug selected", Toast.LENGTH_SHORT)
                        .show();
                break;
            // action with ID action_settings was selected
            case R.id.action_restart:
                Toast.makeText(this, "Restart selected", Toast.LENGTH_SHORT)
                        .show();
                break;
            default:
                break;
        }

        return true;
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        // ignore orientation/keyboard change
        super.onConfigurationChanged(newConfig);
    }

    //LOGIC
    Intent intent  =getIntent();


    /** Called when the user clicks the Send button */
    public void gotoMain(View view) {
        Intent intent = new Intent(this, SessionActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
    }


}