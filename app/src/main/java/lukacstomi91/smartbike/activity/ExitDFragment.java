package lukacstomi91.smartbike.activity;

/**
 * Created by Tom on 2016. 05. 09..
 */
import android.app.ActivityManager;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Switch;

import butterknife.Bind;
import butterknife.ButterKnife;
import lukacstomi91.smartbike.R;
import lukacstomi91.smartbike.ble.BackgroundService;

public class ExitDFragment extends DialogFragment {


    @Bind(R.id.exitFragmentOKButton)
    Button exitFragmentOKButton;

    @Bind(R.id.exitFragmentCancelButton)
    Button exitFragmentCancelButton;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_exit, container,
                false);
        setCancelable(false);
        ButterKnife.bind(this, rootView);

        getDialog().setTitle("Ending Session");


        exitFragmentCancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });

        exitFragmentOKButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent stopS = new Intent(getActivity(), BackgroundService.class);
                getActivity().stopService(stopS);
                boolean answer = isMyServiceRunning(BackgroundService.class);
                Log.e("SMARTBIKK",String.valueOf(answer));

                Intent intent = new Intent(getActivity(), ExitActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
            }
        });

        // Do something else
        return rootView;
    }

    public boolean isMyServiceRunning(Class<?> serviceClass) {
        ActivityManager manager = (ActivityManager) getActivity().getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                return true;
            }
        }
        return false;
    }
}