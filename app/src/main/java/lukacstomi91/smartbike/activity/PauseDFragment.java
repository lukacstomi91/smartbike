package lukacstomi91.smartbike.activity;

/**
 * Created by Tom on 2016. 05. 09..
 */
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Switch;

import butterknife.Bind;
import butterknife.ButterKnife;
import lukacstomi91.smartbike.R;
import lukacstomi91.smartbike.ble.BackgroundService;

public class PauseDFragment extends DialogFragment {


    @Bind(R.id.pauseFragmentOKButton)
    Button pauseFragmentOKButton;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.dialogfragment, container,
                false);
        setCancelable(false);
        ButterKnife.bind(this, rootView);

        getDialog().setTitle("Session Paused");


        pauseFragmentOKButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                BackgroundService.continueLogging();
                dismiss();
            }
        });

        // Do something else
        return rootView;
    }
}