package lukacstomi91.smartbike.activity;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import com.opencsv.CSVReader;
import com.squareup.picasso.Picasso;

import java.io.FileReader;
import java.io.IOException;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import lukacstomi91.smartbike.R;
import lukacstomi91.smartbike.ble.BackgroundService;
import lukacstomi91.smartbike.log.SimpleFileDialog;

/**
 * Created by Tom on 2016. 02. 19..
 */
public class SetupActivity  extends AppCompatActivity
{

    public static int savedWheelRadius = 355;//mm
    public static int savedPedalRadius = 180;//mm

    public static String APP_MODE = "";
    public static int TARGET_AVERAGE_SPEED = 15;

    private TextView hubBattery;
    private TextView wheelBattery;
    private TextView candenceBattery;
    private TextView pedalBattery;
    private TextView USRBattery;

    private TextView StartSession;

    private EditText pace;
    private EditText loadedFile;
    private EditText wheelRadius;
    private EditText pedalRadius;

    private Button loadButton;

    private String m_chosen;

    //Creating handle for ImageViews
    /*
    @Bind(R.id.hubtableMainBattery)
    ImageView hubtableMainBatteryImageView;
    @Bind(R.id.hubtableMidBattery)
    ImageView hubtableMidBatteryImageVIew;
    @Bind(R.id.hubtableRearBattery)
    ImageView hubtableRearBatteryImageVIew;
    */

    private final BroadcastReceiver mGattUpdateReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            final String action = intent.getAction();
            if (BackgroundService.BATTERY_BROADCAST.equals(action)) {
                updateUI(intent.getStringExtra(BackgroundService.BATTERY_BROADCAST));
            }

        }
    };

    private static IntentFilter makeGattUpdateIntentFilter() {
        final IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(BackgroundService.ACTION_GATT_CONNECTED);
        intentFilter.addAction(BackgroundService.ACTION_GATT_DISCONNECTED);
        intentFilter.addAction(BackgroundService.ACTION_GATT_SERVICES_DISCOVERED);
        intentFilter.addAction(BackgroundService.ACTION_DATA_AVAILABLE);
        intentFilter.addAction(BackgroundService.CSC_NOTIFIED);
        intentFilter.addAction(BackgroundService.RSC_NOTIFIED);
        intentFilter.addAction(BackgroundService.HR_NOTIFIED);
        intentFilter.addAction(BackgroundService.BL_NOTIFIED);
        intentFilter.addAction(BackgroundService.ALL_RECEIVED);
        intentFilter.addAction(BackgroundService.DATA_TO_ACTIVITY);
        intentFilter.addAction(BackgroundService.BATTERY_BROADCAST);
        return intentFilter;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setup);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        ButterKnife.bind(this); // Binding layout elements


        getSupportActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
        getSupportActionBar().setCustomView(R.layout.alternative_actionbar);

        final TextView titleFix = (TextView) findViewById(R.id.titlebarText);
        titleFix.setText(R.string.setup_title);

        //loadBitmapsIntoImageViews();

        registerReceiver(mGattUpdateReceiver, makeGattUpdateIntentFilter());

        RadioButton DesiredPace = (RadioButton) findViewById(R.id.setup_radiopace);
        DesiredPace.setChecked(true);
        APP_MODE = "PACE";
        onRadioButtonClicked(DesiredPace);


        hubBattery = (TextView) findViewById(R.id.setup_battery_table_hub_value);
        wheelBattery = (TextView) findViewById(R.id.setup_battery_table_wheel_value);
        candenceBattery = (TextView) findViewById(R.id.setup_battery_table_caldence_value);
        pedalBattery = (TextView) findViewById(R.id.setup_battery_table_pedal_value);
        USRBattery = (TextView) findViewById(R.id.setup_battery_table_usr_value);

        StartSession = (TextView) findViewById(R.id.setup_start);

        pace = (EditText) findViewById(R.id.setup_pace_edit);
        pace.setHint(String.valueOf(TARGET_AVERAGE_SPEED));
        loadedFile = (EditText) findViewById(R.id.setup_loadedfile);

        loadedFile.setFocusable(false);
        wheelRadius = (EditText) findViewById(R.id.setup_wheelvalue);
        wheelRadius.setHint(String.valueOf(savedWheelRadius));
        pedalRadius = (EditText) findViewById(R.id.setup_pedalvalue);
        pedalRadius.setHint(String.valueOf(savedPedalRadius));

        //loadButton = (Button) findViewById(R.id.button_load_race);



    }

    public void loadFile() {
        /////////////////////////////////////////////////////////////////////////////////////////////////
        //Create FileOpenDialog and register a callback
        /////////////////////////////////////////////////////////////////////////////////////////////////
        SimpleFileDialog FileOpenDialog = new SimpleFileDialog(SetupActivity.this, "FileOpen",
                new SimpleFileDialog.SimpleFileDialogListener() {
                    @Override
                    public void onChosenDir(String chosenDir) {
                        // The code in this function will be executed when the dialog OK button is pushed
                        m_chosen = chosenDir;
                        loadedFile.setText(m_chosen);
                        String[] nextLine;
                        String saved = "";
                        int last = 0;
                        try {
                            CSVReader reader = new CSVReader(new FileReader(m_chosen), ',');
                            List myEntries = reader.readAll();
                            String[] lastLine = (String[])myEntries.get(myEntries.size() - 1);

                            String avSpeedSession = lastLine[18];
                            String elapsedTime = lastLine[1];
                            String notify = "Chosen Target Speed: " + avSpeedSession + " km/h \n" +
                                    "Chosen Target Time: " + elapsedTime + "mins";
                            Toast.makeText(SetupActivity.this, notify, Toast.LENGTH_LONG).show();

                            TARGET_AVERAGE_SPEED = Integer.valueOf(avSpeedSession);
                            pace.setText(avSpeedSession);


                            //Log.e("SMITE",lastLine);
                        }



                        catch (IOException e)
                        {
                            Log.e("SMITE",e.toString());
                        }

                    }

                });

        //You can change the default filename using the public variable "Default_File_Name"
        FileOpenDialog.Default_File_Name = "";
        FileOpenDialog.chooseFile_or_Dir();

        /////////////////////////////////////////////////////////////////////////////////////////////////

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // action with ID action_refresh was selected
            case R.id.action_debug:
                Toast.makeText(this, "Debug selected", Toast.LENGTH_SHORT)
                        .show();
                break;
            // action with ID action_settings was selected
            case R.id.action_restart:
                Toast.makeText(this, "Restart selected", Toast.LENGTH_SHORT)
                        .show();
                break;
            default:
                break;
        }

        return true;
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        // ignore orientation/keyboard change
        super.onConfigurationChanged(newConfig);
    }
    /*
    private void loadBitmapsIntoImageViews() {
        Picasso.with(this).load(R.drawable.statusok_compressed).into(hubtableMainBatteryImageView);
        Picasso.with(this).load(R.drawable.statusok_compressed).into(hubtableMidBatteryImageVIew);
        Picasso.with(this).load(R.drawable.statusok_compressed).into(hubtableRearBatteryImageVIew);
    }
    */

    //LOGIC
    Intent intent  =getIntent();


    public void onRadioButtonClicked(View view) {
        // Is the button now checked?
        boolean checked = ((RadioButton) view).isChecked();

        TextView paceEdit = (TextView) findViewById(R.id.setup_pace_edit);
        TextView paceUntis = (TextView) findViewById(R.id.setup_pace_units);

        //Button loadButton = (Button) findViewById(R.id.button_load_race);
        EditText loadedFile = (EditText) findViewById(R.id.setup_loadedfile);


        // Check which radio button was clicked
        switch(view.getId()) {
            case R.id.setup_radiopace:
                if (checked) {
                    paceEdit.setVisibility(View.VISIBLE);
                    paceUntis.setVisibility(View.VISIBLE);

                    //loadButton.setVisibility(View.INVISIBLE);
                    //loadedFile.setVisibility(View.INVISIBLE);
                    loadedFile.setVisibility(View.INVISIBLE);

                    APP_MODE = "PACE";
                    // Pirates are the best
                    break;
                }
            case R.id.setup_radiorace:
                if (checked) {
                    paceEdit.setVisibility(View.VISIBLE);
                    paceUntis.setVisibility(View.VISIBLE);

                    //loadButton.setVisibility(View.VISIBLE);
                    loadedFile.setVisibility(View.VISIBLE);

                    APP_MODE = "RACE";
                    loadFile();

                    // Ninjas rule
                    break;
                }
        }
    }

    public void updateUI(String data) {
        //String batteries = hubBattery + "," +  wheelBattery + "," + cadenceBattery + ","
        //+ powerBattery + "," + USRBattery;
        String[] list = data.split(",");

        hubBattery.setText(list[0]);
        wheelBattery.setText(list[1]);
        candenceBattery.setText(list[2]);
        pedalBattery.setText(list[3]);
        USRBattery.setText(list[4]);

        if ((hubBattery.getText().toString().equals("?"))
                || (wheelBattery.getText().toString().equals("?"))
                || (candenceBattery.getText().toString().equals("?"))
                || (pedalBattery.getText().toString().equals("?"))
                || (USRBattery.getText().toString().equals("?"))) {
            StartSession.setTextColor(getResources().getColor(R.color.disabled));

        } else {
            StartSession.setTextColor(getResources().getColor(R.color.connect_text));
        }
    }



    /** Called when the user clicks the Send button */
    public void gotoSession(View view) {

        if (APP_MODE.equals("PACE")){
            String entry = pace.getText().toString();
            if (entry.equals("")){
                //NO USER ENTRY - HINT IS USED
                TARGET_AVERAGE_SPEED = Integer.valueOf(pace.getHint().toString());
                Log.d("SMARTER", "pace: " + TARGET_AVERAGE_SPEED);
            }else{
                TARGET_AVERAGE_SPEED = Integer.valueOf(entry);
                Log.d("SMARTER", "pace: " + entry);
            }
        }

        String wheelentry = wheelRadius.getText().toString();
        if (wheelentry.equals("")){
            savedWheelRadius = Integer.valueOf(wheelRadius.getHint().toString());
        } else {
            savedWheelRadius = Integer.valueOf(wheelentry);
        }

        String pedalEntry = pedalRadius.getText().toString();
        if (pedalEntry.equals("")){
            savedPedalRadius = Integer.valueOf(pedalRadius.getHint().toString());
        } else {
            savedPedalRadius = Integer.valueOf(pedalEntry);
        }
        Intent intent = new Intent(this, SessionActivity.class);
        startActivity(intent);
    }
}