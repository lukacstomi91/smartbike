package lukacstomi91.smartbike.activity;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;
import butterknife.Bind;
import butterknife.ButterKnife;
import lukacstomi91.smartbike.R;
import lukacstomi91.smartbike.ble.BackgroundService;

/**
 * Created by Tom on 2016. 03. 02..
 */

public class SessionActivity  extends AppCompatActivity
{



    private TextView avSpeed;
    private TextView instSpeed;
    private TextView avCadence;
    private TextView instCadence;
    private TextView gearRatio;
    private TextView temperature;
    private TextView gradient;
    private TextView power;
    private TextView heartRate;
    private TextView time;
    private TextView totalDistance;
    private TextView timingMessage;
    private TextView targetMessage;
    private TextView sessionHint;


    private Button pauseButton;
    private Button finishButton;
    FragmentManager fm = getSupportFragmentManager();

    @Bind(R.id.redBike)
    ImageView redBikeImageView;
    @Bind(R.id.whiteBike)
    ImageView whiteBikeImageView;
    @Bind(R.id.greenBike)
    ImageView greenBikeImageView;
    @Bind(R.id.sessionSpeedAverageSymbol)
    ImageView sessionSpeedAverageSymbolImageView;
    @Bind(R.id.sessionSpeedInstantSymbol)
    ImageView sessionSpeedInstantSymbolImageView;
    @Bind(R.id.sessionCadenceAverageSymbol)
    ImageView sessionCadenceAverageSymbolImageView;
    @Bind(R.id.sessionCadenceInstantSymbol)
    ImageView sessionCadenceInstantSymbolImageView;
    @Bind(R.id.lineDash)
    ImageView lineDashImageView;
    @Bind(R.id.spacerRight)
    ImageView spacerRightImageView;
    @Bind(R.id.spacerMiddle)
    ImageView spacerMiddleImageView;
    @Bind(R.id.spacerLeft)
    ImageView spacerLeftImageView;
    @Bind(R.id.sessionGearRatioSymbol)
    ImageView sessionGearRatioSymbolImageView;
    @Bind(R.id.sessionGearHintSymbol)
    ImageView sessionGearHintSymbolImageView;
    @Bind(R.id.sessionTemperatureSymbol)
    ImageView sessionTemperatureSymbolImageView;
    @Bind(R.id.sessionGradientSymbol)
    ImageView sessionGradientSymbolImageView;
    @Bind(R.id.sessionGearHintValue)
    TextView sessionGearHintValue;
    @Bind(R.id.sessionDistanceSymbol)
    ImageView sessionDistanceSymbolImageView;
    @Bind(R.id.sessionTimeSymbol)
    ImageView sessionTimeSymbolImageView;
    @Bind(R.id.sessionHeartSymbol)
    ImageView sessionHeartSymbolImageView;
    @Bind(R.id.sessionPowerSymbol)
    ImageView sessionPowerSymbolImageView;


    private final BroadcastReceiver mGattUpdateReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            final String action = intent.getAction();
            if (BackgroundService.DATA_TO_ACTIVITY.equals(action)) {
                updateUI(intent.getStringExtra(BackgroundService.DATA_TO_ACTIVITY));
            }

        }
    };

    private static IntentFilter makeGattUpdateIntentFilter() {
        final IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(BackgroundService.ACTION_GATT_CONNECTED);
        intentFilter.addAction(BackgroundService.ACTION_GATT_DISCONNECTED);
        intentFilter.addAction(BackgroundService.ACTION_GATT_SERVICES_DISCOVERED);
        intentFilter.addAction(BackgroundService.ACTION_DATA_AVAILABLE);
        intentFilter.addAction(BackgroundService.CSC_NOTIFIED);
        intentFilter.addAction(BackgroundService.RSC_NOTIFIED);
        intentFilter.addAction(BackgroundService.HR_NOTIFIED);
        intentFilter.addAction(BackgroundService.BL_NOTIFIED);
        intentFilter.addAction(BackgroundService.ALL_RECEIVED);
        intentFilter.addAction(BackgroundService.DATA_TO_ACTIVITY);
        intentFilter.addAction(BackgroundService.BATTERY_BROADCAST);
        return intentFilter;
    }


    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_session);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        ButterKnife.bind(this); // Binding layout elements

        getSupportActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
        getSupportActionBar().setCustomView(R.layout.alternative_actionbar);

        final TextView titleFix = (TextView) findViewById(R.id.titlebarText);
        titleFix.setText(R.string.session_title);

        loadBitmapsIntoImageViews();
        BackgroundService.recordStarted = true;

        avSpeed = (TextView) findViewById(R.id.sessionSpeedAverageValue);
        instSpeed = (TextView) findViewById(R.id.sessionSpeedInstantValue);
        avCadence = (TextView) findViewById(R.id.sessionCadenceAverageValue);
        instCadence = (TextView) findViewById(R.id.sessionCadenceInstantValue);
        gearRatio = (TextView) findViewById(R.id.sessionGearRatioValue);
        temperature = (TextView) findViewById(R.id.sessionTemperatureValue);
        gradient = (TextView) findViewById(R.id.sessionGradientValue);
        power = (TextView) findViewById(R.id.sessionPowerValue);
        heartRate = (TextView) findViewById(R.id.sessionHeartValue);
        time = (TextView) findViewById(R.id.sessionTimeValue);
        totalDistance = (TextView) findViewById(R.id.sessionDistanceValue);
        timingMessage = (TextView) findViewById(R.id.timingMessage);
        targetMessage = (TextView) findViewById(R.id.targetMessage);
        sessionHint = (TextView) findViewById(R.id.sessionHint);


        registerReceiver(mGattUpdateReceiver, makeGattUpdateIntentFilter());

        targetMessage.setVisibility(View.GONE);
        timingMessage.setVisibility(View.VISIBLE);
        /*
        if (SetupActivity.APP_MODE.equals("RACE")){
            targetMessage.setVisibility(View.GONE);
            timingMessage.setVisibility(View.VISIBLE);
        } else {
            timingMessage.setVisibility(View.GONE);
            targetMessage.setText("Target Average Speed: " + String.valueOf(SetupActivity.TARGET_AVERAGE_SPEED + "km\\h"));
            targetMessage.setVisibility(View.VISIBLE);
        }
        */

        pauseButton = (Button) findViewById(R.id.buttonPauseSession);
        finishButton = (Button) findViewById(R.id.buttonEndSession);

        // Capture button clicks
        pauseButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                PauseDFragment PauseDFragment = new PauseDFragment();
                pauseSession();
                PauseDFragment.show(fm, "Dialog Fragment");
            }
        });

        finishButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                EndDFragment EndDFragment = new EndDFragment();
                pauseSession();
                EndDFragment.show(fm, "Dialog Fragment");
            }
        });

    }

    @Override
    public void onBackPressed() {
        //do nothing
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // action with ID action_refresh was selected
            case R.id.action_debug:
                Toast.makeText(this, "Debug selected", Toast.LENGTH_SHORT)
                        .show();
                break;
            // action with ID action_settings was selected
            case R.id.action_restart:
                Toast.makeText(this, "Restart selected", Toast.LENGTH_SHORT)
                        .show();
                break;
            default:
                break;
        }

        return true;
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        // ignore orientation/keyboard change
        super.onConfigurationChanged(newConfig);
    }


    private void loadBitmapsIntoImageViews() {
        Picasso.with(this).load(R.drawable.bike_red_compressed).into(redBikeImageView);
        Picasso.with(this).load(R.drawable.bike_white_compressed).into(whiteBikeImageView);
        Picasso.with(this).load(R.drawable.bike_green_compressed).into(greenBikeImageView);
        Picasso.with(this).load(R.drawable.speed_average_compressed).into(sessionSpeedAverageSymbolImageView);
        Picasso.with(this).load(R.drawable.speed_instant_compressed).into(sessionSpeedInstantSymbolImageView);
        Picasso.with(this).load(R.drawable.cadence_average_compressed).into(sessionCadenceAverageSymbolImageView);
        Picasso.with(this).load(R.drawable.cadence_instant_compressed).into(sessionCadenceInstantSymbolImageView);
        Picasso.with(this).load(R.drawable.line_dash).into(lineDashImageView);
        Picasso.with(this).load(R.drawable.line_left_compressed).into(spacerLeftImageView);
        Picasso.with(this).load(R.drawable.line_middle_compressed).into(spacerMiddleImageView);
        Picasso.with(this).load(R.drawable.line_right_compressed).into(spacerRightImageView);
        Picasso.with(this).load(R.drawable.gear_ratio_compressed).into(sessionGearRatioSymbolImageView);
        Picasso.with(this).load(R.drawable.usr_symbol).into(sessionGearHintSymbolImageView);
        Picasso.with(this).load(R.drawable.temperature_compressed).into(sessionTemperatureSymbolImageView);
        Picasso.with(this).load(R.drawable.angle_compressed).into(sessionGradientSymbolImageView);
        Picasso.with(this).load(R.drawable.distance_compressed).into(sessionDistanceSymbolImageView);
        Picasso.with(this).load(R.drawable.timer_compressed).into(sessionTimeSymbolImageView);
        Picasso.with(this).load(R.drawable.heart_compressed).into(sessionHeartSymbolImageView);
        Picasso.with(this).load(R.drawable.power_compressed).into(sessionPowerSymbolImageView);

    }

    //LOGIC
    Intent intent  =getIntent();


    public void updateUI(String data){
        //String CSVHeaer = "DATAPOINT,ELAPSED_TIME,TOTAL_REV,INST_SPEED(RPM),INST_CADENCE(RPM),SPEED_BATTERY,CADENCE_BATTERY,POWER,POWER_BATTERY," +
        //"USR,USR_BATTERY,HEART_RATE,GRADIENT,TEMPERATURE,HUB_BATTERY,
        // (15)INST_SPEED(KMH),INST_CADENCE(KMH),TIME_IN_SEC,AV_SPEED,AV_CADENCE,ADVANTAGE_IN_SEC,GEAR_RATIO,TOTAL_DIST"


        //05-09 14:09:22.931 15646-15646/lukacstomi91.smartbike E/SMARTBIKERR: 6,00:10,1262,65327,157,151,133,128,40,50,117,140,1,165,97,-24627,-24627,10,-19589,-19589,-13069


        String[] list = data.split(",");



        instSpeed.setText(list[15]);
        instCadence.setText(list[16]);
        temperature.setText(list[13]);
        gradient.setText(list[12]);
        power.setText(list[7]);
        heartRate.setText(list[11]);
        time.setText(list[1]);
        totalDistance.setText(list[22]);

        avSpeed.setText(list[18]);
        avCadence.setText(list[19]);
        gearRatio.setText(list[21]);

        sessionGearHintValue.setText(list[9]);

        String advantage = list[20];

        int heartRateV = Integer.valueOf(list[11]);


        //SHIFT SUGGESTION
        int avCadenceVal = Integer.valueOf(list[19]);
        if (avCadenceVal< 75){
            avCadence.setBackgroundResource(R.drawable.dashed_border_red);

        } else if (76<avCadenceVal && avCadenceVal < 99){
            avCadence.setBackgroundResource(R.drawable.dashed_border_green);
        } else if (100<avCadenceVal) {
            avCadence.setBackgroundResource(R.drawable.dashed_border_red);
        }

        //HEART RATE SUGG
        if (heartRateV< 90){
            heartRate.setBackgroundResource(R.drawable.dashed_border_blue);

        } else if (91<heartRateV && heartRateV < 160){
            heartRate.setBackgroundResource(R.drawable.dashed_border_green);
        } else if (160<heartRateV) {
            heartRate.setBackgroundResource(R.drawable.dashed_border_red);
        }
        
        long advValue = Long.valueOf(advantage);

        String absoluteadvValue = String.valueOf(Math.abs(advValue));


        int averSpeed = Integer.valueOf(list[18]);

        //get bikes right

            if (averSpeed > (Integer.valueOf((SetupActivity.TARGET_AVERAGE_SPEED)+2))){
                redBikeImageView.setVisibility(View.INVISIBLE);
                whiteBikeImageView.setVisibility(View.INVISIBLE);
                greenBikeImageView.setVisibility(View.VISIBLE);

                sessionHint.setText("Go Easy Mate!");
            } else if (Integer.valueOf(SetupActivity.TARGET_AVERAGE_SPEED)-1 < averSpeed && averSpeed < (Integer.valueOf((SetupActivity.TARGET_AVERAGE_SPEED)+1))){
                redBikeImageView.setVisibility(View.INVISIBLE);
                whiteBikeImageView.setVisibility(View.VISIBLE);
                greenBikeImageView.setVisibility(View.INVISIBLE);

                sessionHint.setText("Doing OK!");
            } else if (averSpeed < (Integer.valueOf((SetupActivity.TARGET_AVERAGE_SPEED)-2))){
                redBikeImageView.setVisibility(View.VISIBLE);
                whiteBikeImageView.setVisibility(View.INVISIBLE);
                greenBikeImageView.setVisibility(View.INVISIBLE);
                timingMessage.setText("You are "+ absoluteadvValue + " sec behind!");
                sessionHint.setText("Go Faster!");
            }


        else {

        }

        if (advValue < -3){
            timingMessage.setText("You are "+ absoluteadvValue + " sec behind!");
            redBikeImageView.setVisibility(View.VISIBLE);
            whiteBikeImageView.setVisibility(View.INVISIBLE);
            greenBikeImageView.setVisibility(View.INVISIBLE);

        }else if (-2 < advValue && advValue < 2){
            timingMessage.setText("You are in time!");
            redBikeImageView.setVisibility(View.INVISIBLE);
            whiteBikeImageView.setVisibility(View.VISIBLE);
            greenBikeImageView.setVisibility(View.INVISIBLE);

        }else if (advValue > 3) {
            timingMessage.setText("You are " + advantage + " sec ahead!");
            redBikeImageView.setVisibility(View.INVISIBLE);
            whiteBikeImageView.setVisibility(View.INVISIBLE);
            greenBikeImageView.setVisibility(View.VISIBLE);
        }






    }
    //LOGIC

    /** Called when the user clicks the Send button */
    /*public void gotoEnd() {
        BackgroundService.recordStarted = false;



        Intent intent = new Intent(this, EndActivity.class);
        startActivity(intent);
    }
    */

    public void pauseSession() {
        BackgroundService.pauseLogging();

    }

}