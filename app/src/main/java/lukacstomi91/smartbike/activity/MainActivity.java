package lukacstomi91.smartbike.activity;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothManager;
import android.bluetooth.le.BluetoothLeScanner;
import android.bluetooth.le.ScanCallback;
import android.bluetooth.le.ScanResult;
import android.bluetooth.le.ScanSettings;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.ListView;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

import butterknife.Bind;
import butterknife.ButterKnife;
import lukacstomi91.smartbike.R;
import lukacstomi91.smartbike.ble.BackgroundService;

public class MainActivity extends AppCompatActivity {

    @Bind(R.id.btSwitch)
    Switch btSwitch;

    private static String logTag = "SMARTBIKE_MAIN";
    private Button scanButton;
    private LeDeviceListAdapter mLeDeviceListAdapter;
    private BluetoothAdapter mBluetoothAdapter;
    private boolean mScanning;
    private Handler mHandler;

    private ScanSettings settings;



    private String mDeviceName;
    private String mDeviceAddress;
    private boolean mConnected = false;

    private TextView goButton;


    private static final int REQUEST_ENABLE_BT = 1;
    // Stops scanning after 10 seconds.
    private static final long SCAN_PERIOD = 5000;


    private static IntentFilter mainIntentFilter() {
        final IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(BluetoothAdapter.ACTION_STATE_CHANGED);
        intentFilter.addAction(BackgroundService.ACTION_GATT_CONNECTED);
        intentFilter.addAction(BackgroundService.ACTION_GATT_DISCONNECTED);
        intentFilter.addAction(BackgroundService.ACTION_GATT_SERVICES_DISCOVERED);
        intentFilter.addAction(BackgroundService.ACTION_DATA_AVAILABLE);
        intentFilter.addAction(BackgroundService.CSC_NOTIFIED);
        intentFilter.addAction(BackgroundService.RSC_NOTIFIED);
        intentFilter.addAction(BackgroundService.HR_NOTIFIED);
        intentFilter.addAction(BackgroundService.BL_NOTIFIED);
        intentFilter.addAction(BackgroundService.ALL_RECEIVED);
        return intentFilter;
    }

    private final BroadcastReceiver mReceiver = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {

            String action = intent.getAction();

            // It means the user has changed his bluetooth state.
            if (action.equals(BluetoothAdapter.ACTION_STATE_CHANGED)) {

                if (mBluetoothAdapter.getState() == BluetoothAdapter.STATE_TURNING_OFF) {
                    // The user bluetooth is turning off yet, but it is not disabled yet
                    //mLeDeviceListAdapter.clear();
                    return;
                }

                if (mBluetoothAdapter.getState() == BluetoothAdapter.STATE_OFF) {
                    // The user bluetooth is already disabled.
                    btSwitch.setChecked(false);
                    return;
                }
                    if (mBluetoothAdapter.getState() == BluetoothAdapter.STATE_TURNING_ON) {
                        // The user bluetooth is already disabled.
                        btSwitch.setChecked(true);
                        return;
                }

            }

            else if (BackgroundService.ACTION_GATT_CONNECTED.equals(action)) {
                mConnected = true;
                Log.e(logTag, "ACTION_GATT_CONNECTED");
                //updateConnectionState(R.string.connected);
                //invalidateOptionsMenu();
            } else if (BackgroundService.ACTION_GATT_DISCONNECTED.equals(action)) {
                mConnected = false;
                Log.e(logTag, "ACTION_GATT_DISCONNECTED");
                //updateConnectionState(R.string.disconnected);
                Toast.makeText(MainActivity.this, "Disconnected from Hub!", Toast.LENGTH_SHORT).show();
                mLeDeviceListAdapter.clear();
                //invalidateOptionsMenu();

            } else if (BackgroundService.ACTION_GATT_SERVICES_DISCOVERED.equals(action)) {
                goButton.setTextColor(context.getResources().getColor(R.color.connect_text));
                Toast.makeText(MainActivity.this, "Connected to Hub!", Toast.LENGTH_SHORT).show();
                // Show all the supported services and characteristics on the user interface.
                //onGattServicesConnected(mBluetoothLeService.getSupportedGattServices());
            } else if (BackgroundService.ACTION_DATA_AVAILABLE.equals(action)) {
            }



        }
    };


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);     //  Fixed Portrait orientation
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);


        goButton = (TextView) findViewById(R.id.connect_go);
        /*
        goButton.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {
                                            gotoSetup(goButton);
                                        }
                                    });
        */

        mHandler = new Handler();

        ButterKnife.bind(this); // Binding layout elements


        getSupportActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
        getSupportActionBar().setCustomView(R.layout.alternative_actionbar);

        this.registerReceiver(mReceiver, mainIntentFilter());


        // Initializes a Bluetooth adapter.  For API level 18 and above, get a reference to
        // BluetoothAdapter through BluetoothManager.
        final BluetoothManager bluetoothManager =
                (BluetoothManager) getSystemService(Context.BLUETOOTH_SERVICE);
        mBluetoothAdapter = bluetoothManager.getAdapter();

        // Checks if Bluetooth is supported on the device.
        if (mBluetoothAdapter == null) {
            Toast.makeText(this, R.string.error_bluetooth_not_supported, Toast.LENGTH_SHORT).show();
            finish();
            return;
        }
        else {
            if (!mBluetoothAdapter.isEnabled()) {
                // Bluetooth is not enabled
                btSwitch.setChecked(false);
            } else {
                btSwitch.setChecked(true);
            }
        }








        ListView myList=(ListView)findViewById(R.id.bleDeviceListView);
        mLeDeviceListAdapter = new LeDeviceListAdapter();
        myList.setAdapter(mLeDeviceListAdapter);

        myList.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> l, View v, int position, long id) {

                if (mConnected) {
                    Toast.makeText(MainActivity.this, "You are already connected!", Toast.LENGTH_SHORT).show();
                } else {

                    final BluetoothDevice device = mLeDeviceListAdapter.getDevice(position);
                    if (device == null) return;

                    startBackgroundService(device.getName(), device.getAddress());



                    Log.d("SMARTBIKE", "Item Clicked");
                    if (mScanning) {
                        mBluetoothAdapter.stopLeScan(mLeScanCallback);
                        mScanning = false;
                    }
                    //startActivity(intent);
                }
            }


        });



        // btSwitch = (Switch)findViewById(R.id.btSwitch);
        scanButton = (Button)findViewById(R.id.scanButton);

        //attach a listener to button
        scanButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (mBluetoothAdapter == null) {
                    Toast.makeText(MainActivity.this, R.string.error_bluetooth_not_supported, Toast.LENGTH_SHORT).show();
                    return;
                }
                else {
                    if (!mBluetoothAdapter.isEnabled()) {
                        // Bluetooth is not enabled
                        Toast.makeText(MainActivity.this, "Bluetooth needs to be switched on!", Toast.LENGTH_SHORT).show();
                    } else {



                if ((scanButton.getText().toString()).equals(getString(R.string.connect_button_start_scan))){

                    mLeDeviceListAdapter.clear();
                    scanLeDevice(true);
                    Log.e(logTag, "Scan Started");

                }
                else if ((scanButton.getText().toString()).equals(getString(R.string.connect_button_stop_scan))){
                    scanLeDevice(false);
                    //scanLeDevice(false);

                }
                else{
                    Log.e(logTag,"NOT RECOGNISED");
                    Log.e(logTag,getString(R.string.connect_button_start_scan));
                }

                    }
                }
            }
        });




        //attach a listener to switch
        btSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                if (isChecked) {
                     mBluetoothAdapter.enable();
                    //scanOnClick();
                } else {
                    mBluetoothAdapter.disable();
                }

            }
        });


        ////////////////////////////////////////////BT////////////////////////////


        // Use this check to determine whether BLE is supported on the device.  Then you can
        // selectively disable BLE-related features.
        if (!getPackageManager().hasSystemFeature(PackageManager.FEATURE_BLUETOOTH_LE)) {
            Toast.makeText(this, R.string.ble_not_supported, Toast.LENGTH_SHORT).show();
            finish();
        }




    }


    public boolean startBackgroundService(String deviceName, String deviceAddress) {
        mDeviceName = deviceName;
        mDeviceAddress = deviceAddress;

        Intent gattServiceIntent = new Intent(this, BackgroundService.class);
        //Bundle extra = gattServiceIntent.getExtras();
        gattServiceIntent.putExtra("bleAddress", mDeviceAddress);
        //gattServiceIntent.putExtras(extra);
        startService(gattServiceIntent);
        Log.e(logTag, "STARTING SERVICE?");


        return true;
    }





    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // action with ID action_refresh was selected
            case R.id.action_debug:
                Toast.makeText(this, "Debug selected", Toast.LENGTH_SHORT)
                        .show();
                Intent intent = new Intent(this, DebugActivity.class);
                startActivity(intent);
                break;
            // action with ID action_settings was selected
            case R.id.action_restart:
                Toast.makeText(this, "Restart selected", Toast.LENGTH_SHORT)
                        .show();
                break;
            default:
                break;
        }

        return true;
    }


    ////////////////////////////////////////////BT////////////////////////////


    @Override
    protected void onResume() {
        super.onResume();

        // Ensures Bluetooth is enabled on the device.  If Bluetooth is not currently enabled,
        // fire an intent to display a dialog asking the user to grant permission to enable it.
        if (!mBluetoothAdapter.isEnabled()) {
            if (!mBluetoothAdapter.isEnabled()) {
                Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
                startActivityForResult(enableBtIntent, REQUEST_ENABLE_BT);
            }
        }

        // Initializes list view adapter.
       // ListView myList=(ListView)findViewById(android.R.id.list);
      //  mLeDeviceListAdapter = new LeDeviceListAdapter();
      //  myList.setAdapter(mLeDeviceListAdapter);

        //scanLeDevice(false);
        scanButton.setText(R.string.connect_button_start_scan);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        // User chose not to enable Bluetooth.
        if (requestCode == REQUEST_ENABLE_BT && resultCode == Activity.RESULT_CANCELED) {
            finish();
            return;
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    protected void onPause() {
        super.onPause();
        //scanLeDevice(false);
        mLeDeviceListAdapter.clear();
    }


    private void scanLeDevice(final boolean enable) {

        if (enable) {

                // Stops scanning after a pre-defined scan period.
                //long startTime = System.nanoTime();
                mHandler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        mScanning = false;
                        scanLeDevice(false);
                        //mBluetoothAdapter.stopLeScan(mLeScanCallback);
                        invalidateOptionsMenu();
                    }
                 }, SCAN_PERIOD);

                //long elapsedTime = System.nanoTime()-startTime;
                mScanning = true;
                startScan();
                Log.e(logTag, String.valueOf(mBluetoothAdapter.getState()));

        }
        else {
                mScanning = false;
                Log.e(logTag, String.valueOf(mBluetoothAdapter.getState()));
                stopScan();
        }
        invalidateOptionsMenu();
    }

    private void startScan() {
        int apiVersion = android.os.Build.VERSION.SDK_INT;
        scanButton.setText(R.string.connect_button_stop_scan);
        if (apiVersion > android.os.Build.VERSION_CODES.KITKAT){
            //Log.e(logTag,"KITKAT START SCAN");
            BluetoothLeScanner scanner = mBluetoothAdapter.getBluetoothLeScanner();
            settings = new ScanSettings.Builder()
                    //.setScanMode(ScanSettings.SCAN_MODE_LOW_LATENCY)
                    .setScanMode(ScanSettings.MATCH_MODE_AGGRESSIVE)
                                .build();
            scanner.startScan(scanCallback);
        } else {
            mBluetoothAdapter.startLeScan(mLeScanCallback);
            //Log.e(logTag, "OLD START SCAN");
        }
    }

    private void stopScan() {
        int apiVersion = android.os.Build.VERSION.SDK_INT;
        scanButton.setText(R.string.connect_button_start_scan);
        if (apiVersion > android.os.Build.VERSION_CODES.KITKAT){
            //Log.e(logTag,"KITKAT STOP SCAN");
            BluetoothLeScanner scanner = mBluetoothAdapter.getBluetoothLeScanner();
            scanner.stopScan(scanCallback);
        } else {
            mBluetoothAdapter.stopLeScan(mLeScanCallback);
            //Log.e(logTag, "OLD STOP SCAN");
        }
    }

    // Device scan callback.
    private ScanCallback scanCallback = new ScanCallback() {
        @Override
        public void onScanResult(int callbackType, ScanResult result) {
            onLeScanCallback(result.getDevice());
        }
    };

    private BluetoothAdapter.LeScanCallback mLeScanCallback = new BluetoothAdapter.LeScanCallback() {
        @Override
        public void onLeScan(final BluetoothDevice device, int rssi, byte[] scanRecord) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    onLeScanCallback(device);
                }
            });
        }
    };

    private void onLeScanCallback(BluetoothDevice device) {
        if (device != null) {
            mLeDeviceListAdapter.addDevice(device);
            mLeDeviceListAdapter.notifyDataSetChanged();
        }
    }

    ////////////////////////////////////////////BT////////////////////////////

    //LOGIC

    /** Called when the user clicks the GO button */
    public void gotoSetup(View view) {
        Intent intent = new Intent(this, SetupActivity.class);
        startActivity(intent);
    }



////////////////////////////////////////////BT CONTAINER////////////////////////////



    // Adapter for holding devices found through scanning.
    private class LeDeviceListAdapter extends BaseAdapter {
        private ArrayList<BluetoothDevice> mLeDevices;
        private LayoutInflater mInflator;

        public LeDeviceListAdapter() {
            super();
            mLeDevices = new ArrayList<BluetoothDevice>();
            mInflator = MainActivity.this.getLayoutInflater();
        }

        public void addDevice(BluetoothDevice device) {
            if (!mLeDevices.contains(device)) {
                mLeDevices.add(device);
            }
        }

        public BluetoothDevice getDevice(int position) {
            return mLeDevices.get(position);
        }

        public void clear() {
            mLeDevices.clear();
        }

        @Override
        public int getCount() {
            return mLeDevices.size();
        }

        @Override
        public Object getItem(int i) {
            return mLeDevices.get(i);
        }

        @Override
        public long getItemId(int i) {
            return i;
        }

        @Override
        public View getView(int i, View view, ViewGroup viewGroup) {
            ViewHolder viewHolder;
            // General ListView optimization code.
            if (view == null) {
                view = mInflator.inflate(R.layout.listitem_device, null);
                viewHolder = new ViewHolder();
                viewHolder.deviceAddress = (TextView) view.findViewById(R.id.device_address);
                viewHolder.deviceName = (TextView) view.findViewById(R.id.device_name);
                view.setTag(viewHolder);
            } else {
                viewHolder = (ViewHolder) view.getTag();
            }

            BluetoothDevice device = mLeDevices.get(i);
            final String deviceName = device.getName();
            if (deviceName != null && deviceName.length() > 0)
                viewHolder.deviceName.setText(deviceName);
            else
                viewHolder.deviceName.setText(R.string.unknown_device);
            viewHolder.deviceAddress.setText(device.getAddress());

            return view;
        }
    }

    static class ViewHolder {
        TextView deviceName;
        TextView deviceAddress;
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults){
        if(requestCode == 123456789)
        {
            //... //Do something based on grantResults
        }
    }
}

